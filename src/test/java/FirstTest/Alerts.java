package FirstTest;


import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Alerts {

    WebDriver driver;
    JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;

    @BeforeClass
    public void BeforeClass() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");

    }

    @AfterClass
    public void AfterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void clickForOk() {
        clickOnButtonJSConfirm ();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");
    }

    @Test
    public void clickForCancel() {
        clickOnButtonJSConfirm ();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    @Test
    public void clickForPromptOkSend() {
        clickOnButtonJSPrompt();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("QA Kostina");
        alert.accept();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: QA Kostina");
    }
    @Test
    public void clickForPromptOk() {
        clickOnButtonJSPrompt();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    @Test
    public void clickForPromptCancelSend() {
        clickOnButtonJSPrompt();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("QA Kostina");
        alert.dismiss();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }
    @Test
    public void clickForPromptCancel() {
        clickOnButtonJSPrompt();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }
    public WebElement clickOnButtonJSConfirm(){
        return clickOnButtonByText( ButtonsOnPage.CONFIRM.getTextOnButton());
    }
    public void clickOnButtonJSConfirmJS(){
        clickOnButtonByTextWithJS(ButtonsOnPage.CONFIRM);
    }
    public WebElement clickOnButtonJSPrompt(){
        return clickOnButtonByText( ButtonsOnPage.PROMPT.getTextOnButton());
     }
    public void clickOnButtonJSPromptJS(){
        clickOnButtonByTextWithJS(ButtonsOnPage.PROMPT);
    }


    private WebElement clickOnButtonByText(String textOnButton){
        WebElement button = driver.findElement(By.xpath("//button[text()='%s']".formatted(textOnButton)));
        button.click();
        return button;

    }

    enum ButtonsOnPage {
        CONFIRM("Click for JS Confirm"),
        PROMPT("Click for JS Prompt");

        private String textOnButton;

        ButtonsOnPage(String textOnButton) {
            this.textOnButton = textOnButton;
        }

        public String getTextOnButton() {
            return textOnButton;
        }
    }

        private void clickOnButtonByTextWithJS (ButtonsOnPage buttonsOnPage) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        String script = "";
        switch (buttonsOnPage) {
            case CONFIRM -> script = "jsConfirm();";
            case PROMPT -> script = "jsPrompt();";
        }
        javascriptExecutor.executeScript(script);
    }
    @Test
    public void clickForOkJS() {
        clickOnButtonJSConfirmJS ();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Ok");
    }

    @Test
    public void clickForCancelJS() {
        clickOnButtonJSConfirmJS();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS Confirm");
        Assert.assertEquals(resultText, "You clicked: Cancel");
    }

    @Test
    public void clickForPromptOkSendJS() {
        clickOnButtonJSPromptJS();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("QA Kostina");
        alert.accept();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: QA Kostina");
    }
    @Test
    public void clickForPromptOkJS() {
        clickOnButtonJSPromptJS();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered:");
    }

    @Test
    public void clickForPromptCancelSendJS() {
        clickOnButtonJSPromptJS();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.sendKeys("QA Kostina");
        alert.dismiss();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }
    @Test
    public void clickForPromptCancelJS() {
        clickOnButtonJSPromptJS();
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();

        WebElement result = driver.findElement(By.id("result"));
        String resultText = result.getText();

        Assert.assertEquals(alertText, "I am a JS prompt");
        Assert.assertEquals(resultText, "You entered: null");
    }

}
